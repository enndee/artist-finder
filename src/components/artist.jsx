import React from 'react';

class Artist extends React.Component {
    constructor(){
        super();
        this.state={ 
            imageLoaded: false,
            imageError: false, 
            imageAlt: ''
        }
        
        this.onImageLoad = this.onImageLoad.bind(this);
        this.onImageError = this.onImageError.bind(this);
    }
   
    onImageLoad(){
        this.setState({
            imageLoaded: true,
            imageAlt: this.props.artistInfo.artistName
        });
    }

    onImageError(){
        this.setState({
            imageError: true,
            imageLoaded: true,
            imageAlt: ''            
        })
    }

    // Image is set to 'hide' initially, then on load the class is changed from 'hide' to 'fade-in'. 
    // This is done as just setting 'fade-in' means that the animation runs straight away even if the picture takes time to download and so the image appears instantly when it is finally downloaded.
    // Just setting the class to 'fade-in' (with no 'hide' in place) when onLoad runs means the picture appears briefly (i.e. loads!) and then the fade animation starts, which obviously looks rubbish.
    render() {
        
        const {thumbnail, artistName, artistWebsite, artistBio} = this.props.artistInfo;
        let thumbClass = '';
        let imageClass = 'hide';

        if (!thumbnail || this.state.imageError){
            thumbClass = ' image-error';            
        }

        if (this.state.imageLoaded){
            imageClass = 'fade-in';
        }

        return (
            <div className="artist-container">
                <div className={'artist-thumbnail' + thumbClass}>

                    {/* If there is no thumbnail specified in the json then dispaly a 'no image' message. Otherwise check if the image has loaded yet.  */}
                    { thumbnail ? 
                        (this.state.imageLoaded ? '' : <div className="image-loading">Image Loading...</div>)
                    :
                        <div className="">No image available</div>
                    }
                    
                    {this.state.imageError ? 'Image could not be found' : null }               

                    <img className={imageClass} alt={this.state.imageAlt} src={thumbnail} onLoad={this.onImageLoad} onError={this.onImageError} /> 

                </div>
                <div className="artist-info fade-in">
                    <h1>{artistName}</h1>                   
                    
                    <h2>Website</h2>
                    <p>
                        {artistWebsite ? <a href={'http://' + artistWebsite}>{artistWebsite}</a> : 'N/A'}
                    </p>
                    
                    <h2>Biography</h2>
                    <p>
                        {artistBio}
                    </p>
                </div>
            </div>
        );
    }
}

export default Artist;