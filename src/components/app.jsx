import React from 'react';
import Search from './search';
import Artist from './artist';

class App extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            artistInfo: { 
                artistName: null,
                artistBio: null,
                artistWebsite: null,
                thumbnail: null
            },
            loading: true,
            errorMessage: null           
        }

        this.handleSearchTerm = this.handleSearchTerm.bind(this);

    }

    getArtistInfo(artist = 'U2') {

        const url = 'https://www.theaudiodb.com/api/v1/json/1/search.php?s=' + artist;        

        fetch(url)
            .then(response => {
                if (response.ok) {
                    return response.json()
                } else {
                    return Promise.reject({
                        status: response.status,
                        statusText: response.statusText
                    })
                }
            })
            .then(data => {                
                
                // Is the data returned null or not?
                // If it is null then the artist isn't in the database
                if (data.artists) {

                    const result = data.artists[0];
                    
                    this.setState({
                        artistInfo:
                        {
                            artistName: result.strArtist,
                            artistBio: result.strBiographyEN,
                            artistWebsite: result.strWebsite,
                            thumbnail: result.strArtistThumb
                        },
                        loading: false,
                        errorMessage: null
                    });
                }
                else {
                    this.setState({
                        errorMessage: "We can't find that particular artist",
                        loading: false
                    });
                }
                
            })
            .catch(error => {
                if (error.status === 404) {
                    this.setState({
                        errorMessage: "There is a problem connecting to the database",
                        loading: false
                    
                    });
                }
            })

    }

    componentDidMount(){
        this.getArtistInfo();        
    }

    handleSearchTerm(searchTerm) {
        this.setState({loading: true});
        this.getArtistInfo(searchTerm);
    }

    // In the render function, check to see if the data is loading (i.e. a response has not been received from the API yet) If we are still waiting on the data then display a 'loading' message.
    // If it's not loading then that means we have gotten a response. Check to see if there was an error in the response. If so, display the error. If there is no error then display the data.
    render() {
        return (
            <div className="app-container"> 
                <Search handleSearchTerm={this.handleSearchTerm} />                            
                {this.state.loading ? <div className="loading-container">Loading...</div> 
                : 
                (this.state.errorMessage ? <div className="loading-container error">{this.state.errorMessage}</div> : <Artist artistInfo={this.state.artistInfo} /> )}
            </div>
        );
    }
}

export default App;