import React from "react";

class Search extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchTerm: ""
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ searchTerm: event.target.value });
  }

  handleSubmit(event) {
    //alert('Search term is: ' + this.state.searchTerm);
    event.preventDefault();
    this.props.handleSearchTerm(this.state.searchTerm);
  }

  render() {
    return (
      <div className="search-container">
        <form className="flex-form" onSubmit={this.handleSubmit}>
          <input
            className="search-field"
            type="text"
            value={this.state.value}
            onChange={this.handleChange}
            aria-label="Enter artist name"
            placeholder="Enter artist name..."
            autoCorrect="off"
          />
          <input className="search-button" type="submit" value="Search" />
        </form>
        <div className="logo">Artist Finder</div>
      </div>
    );
  }
}

export default Search;
